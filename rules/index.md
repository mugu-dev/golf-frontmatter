# Code Golf Rules and Regulations

## Build Target

**OS**: Linux 5.x kernel

**Hardware**: Intel Haswell, 2 CPU cores, 2GB RAM

**Language**: ISO C17

**Compiler**: LLVM/Clang 13.0.1

**Locale**: en_US.UTF-8

## Compiling

Code will be built using the following commands unless otherwise specified in the instructions of a particular task:

For smallest binary footprint:
```sh
clang -std=c17 -Os -march=native -o footprint main.c -lm
```

For all other metrics:
```sh
clang -std=c17 -O3 -march=native -mtune=native main.c -lm
```

> NOTE: Source files should be UTF-8 encoded to ensure proper compilation and scoring.

## Timing
TBD

## Rounds

Each "round" of code golf will be comprised of either 9 or 18 tasks, called "holes".

While a given player may score higher or lower than others on an individual hole, the sum total of their scores for each hole determines the winner of the round.

## Scoring
All holes will be scored on three metrics:
1. [**Source Footprint**](#source-footprint)
1. [**Binary Footprint**](#binary-footprint)
1. [**Walltime**](#walltime)

Some holes will also include [bonus metrics](#bonus-scoring) selected from a pool of predetermined options.

### Source Footprint

The file size of the source code, _in bytes_. Smaller is better.

- Non-quoted whitespace will be subtracted from the total score.
- Multi-byte characters are allowed and will be counted in 

### Binary Footprint

The file size of the compiled binary, _in bytes_. Smaller is better.

- The size will be determined using `du -b --apparent-size` on the resulting binary.

### Walltime

The elapsed walltime taken to execute the program, _precision TBD_. Smaller is better.

### Bonus scoring

TBD

Bonus metrics are irregular metrics which may be added or subtracted from the player's score. Subtractive metrics **(-)** reduce the player's score, while additive metrics **(+)** penalize the player and increase their score.

Example bonus metrics:
- Highest level of `-Werror` without failure. ($`-5`$) for `-Werror`, ($`-10`$) for `-Wall -Werror`, ($`-15`$) for `-Weverything -Werror`
- Number of emojis in source code. ($`-4 \times N`$)
- Number of preprocessor statements. ($`+N`$)
- Total number of syscalls _as detected by `strace`_. ($`+N`$)


